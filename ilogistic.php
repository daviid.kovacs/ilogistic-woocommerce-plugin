<?php
/**
 * Plugin Name: iLogistic Fulfillment Order and Product Sync
 * Plugin URI: https://ilogistic.eu
 * Description: This plugins syncs all Woocommerce orders and products to the iLogistic warehouse manager software.
 * Version: 1.6.7
 * Author: Cyclick Kft.
 * Author URI: https://cyclick.hu
 * License: GPLv3
 * WC requires at least: 4.0.0
 * WC tested up to: 5.3.0
 * Requires PHP: 7.3
 */

use Ilogistic\Autoloader;
use Ilogistic\Ilogistic_Woo_App;

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}
/**
 * Inits the autoloader
 */
require_once plugin_dir_path( __FILE__ ) . 'src/Autoloader.php';
new Autoloader( plugin_dir_path( __FILE__ ) . "src/" );

register_activation_hook( __FILE__, Ilogistic_Woo_App::class . '::activation_handler' );

register_deactivation_hook( __FILE__, Ilogistic_Woo_App::class . '::deactivation_handler' );

$app = Ilogistic\Ilogistic_Woo_App::get_instance();
$app->run();
