# 1.6.7

Fixed condition that determines product is new.

# 1.6.6

Fixed an issue with checking for status updates only affected the first 10 orders.

# 1.6.5

Fixed PHP exception upon using `push_product()` function and receiving `WP_Error` object as response. (Thrown when WP
can't reach the target host.)

# 1.6.4

Fixed null return value on remote API requests to update stocks.

# 1.6.3

Tested with WooCommerce 5.0. Added 3 sec leeway for the validation in the order creation, details attached to some
events as logs.

# 1.6.2

Fixed that stock update only applies for the first page of products, instead of every product.

# 1.6.1

Added new field for storing WooCommerce ID, added new status checks for newly created orders.

# 1.5.4

Fixed `null` calls and failing token generation errors.

# 1.5.3

Fixed shipping properties update from the WooCommerce to the iLogistic.

# 1.5.2

Fixed setting product stock to 0 when no data returned, but fetaure enabled.

# 1.5.1

Fixed settings buttons on versioned links.

# 1.5.0

Introduced a new feature, update stock managed product's stocks.

# 1.4.1

Updated `Jóváhagyva/Jóváhagyásra vár` status, to a multiselect.

# 1.4.0

Introduced a new feature to upload orders that has been submitted before the installation. It has a settings option to
enable this feature.

# 1.3.0

Introduced custom log file, with logfmt formated messages.

# 1.2.4

Improved logging capabilities.

# 1.2.3

Hotfix: Updated link position under the Plugins tab.

# 1.2.2

Hotfix: Removed `JSON_NUMERIC_CHECK` option, forced string cast on SKUs to avoid bad conversion.

# 1.2.1

Hotfix: Removed exit from redirect.

# 1.2.0

Added new settings menu to plugin settings and settings redirection to plugin activation.

# 1.1.1

Fixed default or bad shipping/payment method handling.

# 1.1.0

Added handling of custom Shipping and Payment methods.

# 1.0.2

Shipping data filled out for iLogistic system on digital orders as well.

# 1.0.1

Shipping company added for orders with only digital products.

# 1.0.0

Initial release.