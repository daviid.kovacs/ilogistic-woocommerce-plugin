=== iLogistic Fulfillment WooCommerce Plugin ===

Tags: fulfillment, ilogistic, woocommerce

Requires PHP: 7.3

Requires at least: 5.4.0

License: GPLv3

[License URI](LICENSE)

== Description ==

Az iLogistic egy Fulfillment As A Service szolgáltás, mely akár te webshop-odhoz tartozó raktár készlet kezelését,
illetve csomagolást és csomag feladást hivatott elvégezni.

A teljes dokumentációért látogasson el
az [iLogistic API-okkal kapcsolatos weboldalára.](https://api.ilogistic.eu/documentation)

A plugin használatához rendelkeznie kell, egy regisztrált és aktivált iLogistic felhasználóval.

Bármilyen sérülékenységet, hibát talál, vagy csak kérdése van, keressen minket bizalommal a
következő [elérhetőségeken](http://api.ilogistic.eu/documentation/help).