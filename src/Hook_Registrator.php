<?php


namespace Ilogistic;


final class Hook_Registrator {
	/**
	 * Calls every 'register' function from the classes that injected as params
	 *
	 * @param string ...$classes Takes any number of class names
	 */
	public static function register_hooks( ...$classes ) {
		foreach ( $classes as $class ) {
			call_user_func( [ $class, 'register' ] );
		}
	}

	/**
	 * Calls every 'unregister' function from the classes that injected as params
	 *
	 * @param string ...$classes Takes any number of class names
	 */
	public static function unregister_hooks( ...$classes ) {
		foreach ( $classes as $class ) {
			call_user_func( [ $class, 'unregister' ] );
		}
	}
}