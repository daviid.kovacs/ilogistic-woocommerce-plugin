<?php


namespace Ilogistic\Hooks;

use Ilogistic\Api_Service;
use Ilogistic\Ilogistic_Woo_App;
use Ilogistic\Logfmt_Log_Handler_File;
use Ilogistic\Model\Product;
use WC_Logger;
use WC_Product;

class Product_Hooks {
	/**
	 * Registers the hooks.
	 */
	public static function register() {
		add_action( 'woocommerce_update_product', self::class . '::create_ilogistic_product', 10, 1 );
		add_action( 'wp', self::class . '::ilogistic_update_trigger' );
		add_action( 'ilogistic_wc_update_stock', self::class . '::update_all_product_stock' );
	}

	/**
	 * @param int $product_id WC_Product The created/updated WooCommerce product ID
	 * Gets the new product and send it to the API, if there is a cost and SKU field.
	 * Happens when a WC product is created/modified.
	 */
	public static function create_ilogistic_product( int $product_id ) {
		$log_handler = new Logfmt_Log_Handler_File();
		$logger      = new WC_Logger( [ $log_handler ] );
		$product     = wc_get_product( $product_id );
		// do something with this product
		if ( $product->get_sku() === '' ) {
			$logger->alert(
				"Failed to push product {$product_id}, no SKU presented.",
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);

			return;
		}
		if ( $product->get_price() === '' ) {
			$logger->warning(
				"The product {$product_id} was sent with price 0!",
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);
		}
		$updated_date = $product->get_date_modified();
		$created_date = $product->get_date_created();
		if ( is_null( $updated_date ) === false && is_null( $created_date ) === false ) {
			if ( $updated_date->getTimestamp() - $created_date->getTimestamp() <= 3 ) {
				$parsed_product = Product::create_from_woocommerce_product( $product );
				Api_Service::getService()->push_product( $parsed_product, esc_attr( get_option( 'ilogistic_token' ) ) );
			}
		} else {
			$logger->alert( "Failed to push product {$product_id}, created or updated date is not presented.",
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);
		}
	}

	/**
	 * Gets every WC_Product and generate an iLogistic product from it, then pushes it to the API
	 */
	public static function register_all_products() {
		$args                    = array( 'return' => 'ids', 'posts_per_page' => - 1 );
		$product_ids             = wc_get_products( $args );
		$ilogistic_product_array = array();
		foreach ( $product_ids as $product_id ) {
			$product = new WC_Product( $product_id );
			if ( $product->get_sku() !== '' ) {
				array_push(
					$ilogistic_product_array,
					Product::create_from_woocommerce_product( $product )
				);
			}
		}
		$api_service = Api_Service::getService();
		$api_service->push_products( $ilogistic_product_array, esc_attr( get_option( 'ilogistic_token' ) ) );
	}

	/**
	 * Creates an event that triggered to run every 5 minutes (see above)
	 */
	public static function ilogistic_update_trigger() {
		if ( ! wp_next_scheduled( 'ilogistic_wc_update_stock' ) ) {
			wp_schedule_event( time(), 'every_5_min', 'ilogistic_wc_update_stock' );
		}
	}

	/**
	 * Updates all the public, stock managed product's stock.
	 */
	public static function update_all_product_stock() {
		$args               = array(
			'status'         => 'publish',
			'return'         => 'objects',
			'manage_stock'   => true,
			'posts_per_page' => - 1
		);
		$managed_products   = wc_get_products( $args );
		$updatable_products = array();
		$log_handler        = new Logfmt_Log_Handler_File();
		$logger             = new WC_Logger( [ $log_handler ] );
		$logger->info(
			"Product stock sync started!",
			Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
		);
		/**
		 * @var WC_Product $product
		 */
		foreach ( $managed_products as $key => $product ) {
			if ( $product->get_sku() !== '' ) {
				array_push( $updatable_products, $product->get_sku() );
			} else {
				unset( $managed_products[ $key ] );
				$logger->info(
					"Product {$product->get_id()} was not eligible for stock update!",
					Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT + [ 'reason' => '"no sku"' ]
				);
			}
		}
		/**
		 * ['sku' => 'stock']
		 */
		$api_responses = Api_Service::getService()->get_stock(
			$updatable_products,
			esc_attr( get_option( 'ilogistic_token' ) )
		);
		foreach ( $managed_products as $product ) {
			$sku          = $product->get_sku();
			$prevQuantity = $product->get_stock_quantity();
			if ( is_null( $api_responses[ $sku ] ) === false ) {
				$product->set_stock_quantity( $api_responses[ $sku ] );
				$logger->info(
					"Updated {$sku}'s stock from {$prevQuantity}, to {$api_responses[ $sku ]}",
					Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
				);
			} else {
				$logger->info(
					"Product {$product->get_id()} was not eligible for stock update!",
					Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT + [ 'reason' => '"no response"' ]
				);
			}
			$product->save();
		}
	}
}