<?php


namespace Ilogistic\Hooks;


class Admin_Menu_Hooks {
	/**
	 * Registers all the hooks.
	 */
	public static function register() {
		add_filter( 'plugin_action_links', self::class . '::add_settings_link', 1, 2 );
		add_filter( 'woocommerce_settings_tabs_array', self::class . '::add_settings_tab', 50 );
		add_action( 'woocommerce_settings_tabs_ilogistic_settings_tab', self::class . '::settings_tab' );
		add_action( 'woocommerce_update_options_ilogistic_settings_tab', self::class . '::update_settings' );
	}

	/**
	 * Adds a new array element to the settings array.
	 *
	 * @param $settings_tabs array Settings tabs
	 *
	 * @return array Modified settings tabs.
	 */
	public static function add_settings_tab( array $settings_tabs ): array {
		$settings_tabs['ilogistic_settings_tab'] = __( 'iLogistic', 'woocommerce-settings-tab-ilogistic' );

		return $settings_tabs;
	}

	/**
	 * Registers what are the fields.
	 */
	public static function settings_tab() {
		woocommerce_admin_fields( self::get_settings() );
	}

	/**
	 * Registers what fields should be updating on save.
	 */
	public static function update_settings() {
		woocommerce_update_options( self::get_settings() );
	}

	/**
	 * Contains the settings for the tabs.
	 * @return array Returns the filtered settings
	 */
	public static function get_settings(): array {
		$order_statuses = wc_get_order_statuses();
		$settings       = array(
			'section_title'                => array(
				'name' => 'iLogistic Token beállítások',
				'type' => 'title',
				'desc' => '',
				'id'   => 'wc_settings_tab_ilogistic_token_section_title'
			),
			'ilogistic_api_key'            => array(
				'name' => 'API kulcs',
				'type' => 'text',
				'desc' => 'A beépülő használatához, szükség van egy iLogistic API kulcsra ' .
				          'melyet a következő url-el érhet el: [URL]',
				'id'   => 'ilogistic_token'
			),
			'ilogistic_push_create_before' => array(
				'name' => 'Telepítés előtti rendelések automatikus feltöltése',
				'type' => 'checkbox',
				'desc' => 'Amennyiben ez a funkció aktív, feltölti az iLogistic felhasználójába azokat a rendeléseket' .
				          ' melyek a plugin telepítése előtt lettek létrehozva ' .
				          'és `Jóváhagyva/Jóváhagyásra vár`-nak megfeleltetett státuszban vannak.',
				'id'   => 'ilogistic_push_before_install'
			),
			'section_end'                  => array(
				'type' => 'sectionend',
				'id'   => 'wc_settings_tab_ilogistic_token_section_end'
			),
			'status_section_title'         => array(
				'name' => 'iLogistic státusz beállítások',
				'type' => 'title',
				'desc' => '',
				'id'   => 'wc_settings_tab_ilogistic_status_section_title'
			),
			'ilogistic_status_1'           => array(
				'name'    => 'Jóváhagyva/Jóváhagyásra vár',
				'type'    => 'multiselect',
				'options' => $order_statuses,
				'desc'    => 'A `Jóváhagyva/Jóváhagyásra vár` iLogistic státuszban lévő rendelésekkel ' .
				             'egyenértékű státusz',
				'id'      => 'ilogistic_status_jovahagyva'
			),
			'ilogistic_status_2'           => array(
				'name'    => 'Összekészítés alatt',
				'type'    => 'select',
				'options' => $order_statuses,
				'desc'    => 'A `Összekészítés alatt` iLogistic státuszban lévő rendelésekkel egyenértékű státusz',
				'id'      => 'ilogistic_status_osszekeszites'
			),
			'ilogistic_status_3'           => array(
				'name'    => 'Csomagolva',
				'type'    => 'select',
				'options' => $order_statuses,
				'desc'    => 'A `Csomagolva` iLogistic státuszban lévő rendelésekkel egyenértékű státusz',
				'id'      => 'ilogistic_status_csomagolva'
			),
			'ilogistic_status_4'           => array(
				'name'    => 'Futárnak átadva',
				'type'    => 'select',
				'options' => $order_statuses,
				'desc'    => 'A `Futárnak átadva` iLogistic státuszban lévő rendelésekkel egyenértékű státusz',
				'id'      => 'ilogistic_status_futarnak'
			),
			'ilogistic_status_5'           => array(
				'name'    => 'Kiküldve',
				'type'    => 'select',
				'options' => $order_statuses,
				'desc'    => 'A `Kiküldve` iLogistic státuszban lévő rendelésekkel egyenértékű státusz',
				'id'      => 'ilogistic_status_kikuldve'
			),
			'status_section_end'           => array(
				'type' => 'sectionend',
				'id'   => 'wc_settings_tab_ilogistic_status_section_end'
			),
			'shipping_section_title'       => array(
				'name' => 'iLogistic szállítási mód beállítások',
				'type' => 'title',
				'desc' => '',
				'id'   => 'wc_settings_tab_ilogistic_status_section_title'
			),
			'ilogistic_shipping_gls'       => array(
				'name' => 'GLS',
				'type' => 'text',
				'desc' => 'A `GLS` iLogistic szállítási módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.',
				'id'   => 'ilogistic_shipping_gls'
			),
			'ilogistic_shipping_foxpost'   => array(
				'name' => 'Foxpost',
				'type' => 'text',
				'desc' => 'A `Foxpost` iLogistic szállítási módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.' .
				          'Figyelem!: ennek megfeleltetett rendeléseknek tartalmaznia kell a ' .
				          '`foxpost_woo_parcel_apt_id` egyedi mezőt melyet a Foxpost plugin tölt ki!',
				'id'   => 'ilogistic_shipping_foxpost'
			),
			'shipping_section_end'         => array(
				'type' => 'sectionend',
				'id'   => 'wc_settings_tab_ilogistic_shipping_section_end'
			),
			'payment_section_title'        => array(
				'name' => 'iLogistic fizetési mód beállítások',
				'type' => 'title',
				'desc' => '',
				'id'   => 'wc_settings_tab_ilogistic_status_section_title'
			),
			'ilogistic_payment_card'       => array(
				'name' => 'Kártyás fizetés',
				'type' => 'text',
				'desc' => 'A `Kártyás fizetés` iLogistic fizetési módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.',
				'id'   => 'ilogistic_payment_card'
			),
			'ilogistic_payment_cod'        => array(
				'name' => 'Utánvétes fizetés',
				'type' => 'text',
				'desc' => 'A `Utánvétes fizetés` iLogistic fizetési módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.',
				'id'   => 'ilogistic_payment_cod'
			),
			'ilogistic_payment_transfer'   => array(
				'name' => 'Átutalásos fizetés',
				'type' => 'text',
				'desc' => 'A `Átutalásos fizetés` iLogistic fizetési módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.',
				'id'   => 'ilogistic_payment_transfer'
			),
			'payment_section_end'          => array(
				'type' => 'sectionend',
				'id'   => 'wc_settings_tab_ilogistic_payment_section_end'
			),
		);

		return apply_filters( 'wc_settings_tab_ilogistic_settings', $settings );
	}

	/**
	 * Adds a new 'Beállítások' option, under the plugin on the plugins tab.
	 *
	 * @param array $actions Current links under the plugin's plugin record.
	 * @param string $plugin_file_name Plugin's name
	 *
	 * @return array Modified links of the plugin's plugin record.
	 */
	public static function add_settings_link( array $actions, string $plugin_file_name ): array {
		if (
		preg_match( '/^(ilogistic-woocommerce-plugin[a-zA-Z0-9._-]*)(\/ilogistic.php)$/', $plugin_file_name )
		) {
			array_unshift(
				$actions,
				'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=ilogistic_settings_tab' ) . '">' .
				'Beállítások</a>'
			);
		}

		return $actions;
	}


}