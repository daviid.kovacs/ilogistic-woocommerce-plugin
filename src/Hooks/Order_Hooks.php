<?php

namespace Ilogistic\Hooks;


use Ilogistic\Api_Service;
use Ilogistic\Ilogistic_Woo_App;
use Ilogistic\Logfmt_Log_Handler_File;
use Ilogistic\Model\Order;
use WC_Logger;
use WC_Order;
use WC_Product;

class Order_Hooks {

	private static $logger = null;

	/**
	 * Registers the hooks.
	 */
	public static function register() {
		add_action( 'woocommerce_thankyou', self::class . '::create_ilogistic_order' );
		add_filter( 'cron_schedules', self::class . '::create_every_5_min_cron' );
		add_action( 'wp', self::class . '::ilogistic_update_trigger' );
		add_action( 'ilogistic_wc_update_order', self::class . '::update_ilog_order_event' );
		add_action( 'before_delete_post', self::class . '::delete_order_event' );
		add_action( 'ilogistic_wc_update_order', self::class . '::update_wc_order_event' );
		add_action( 'ilogistic_wc_update_order', self::class . '::push_wc_order_created_before_install' );
	}

	private static function getLogger(): WC_Logger {
		if ( is_null( self::$logger ) ) {
			$log_handler  = new Logfmt_Log_Handler_File();
			self::$logger = new WC_Logger( [ $log_handler ] );
		}

		return self::$logger;
	}

	/**
	 * Creates a new order and send it to the iLogistic API when a WC order is created.
	 * (The created iLogistic order's id will be injected to the order as an 'ilogistic_id' custom field.)
	 *
	 * @param int $order_id
	 */
	public static function create_ilogistic_order( int $order_id ) {
		$order             = new WC_Order( $order_id );
		$isProductEmptySku = false;
		if ( $order->get_meta( 'ilogistic_id', true ) !== '' ) {
			self::getLogger()->alert(
				"Failed to push order {$order_id}, the order is already exists.",
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);

			return;
		}
		foreach ( $order->get_items() as $item ) {
			$product_data = new WC_Product( $item->get_data()['product_id'] );
			if ( $product_data->get_sku() === "" ) {
				$isProductEmptySku = true;
			}
			unset( $product_data );
		}
		if ( $isProductEmptySku ) {
			self::getLogger()->alert(
				"Failed to push order {$order_id}, the order contains a product without SKU.",
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);

			return;
		}
		$parsed_order = Order::create_from_woocommerce_order( $order );
		$api_service  = Api_Service::getService();
		$ilogistic_id = $api_service->push_order(
			$parsed_order,
			$order_id,
			esc_attr( get_option( 'ilogistic_token' ) )
		);
		if ( $ilogistic_id !== '' ) {
			$order->add_meta_data( 'ilogistic_id', $ilogistic_id );
			$order->save();
		}
	}

	/**
	 * Adds a new cron job to the cron job schedules which will run every 5 min.
	 *
	 * @param $schedules array Current cron schedules
	 *
	 * @return array Updated cron schedules
	 */
	public static function create_every_5_min_cron( array $schedules ): array {
		$schedules['every_5_min'] = array(
			'interval' => 300,
			'display'  => __( 'Every 5 minutes' )
		);

		return $schedules;
	}

	/**
	 * Creates an event that triggered to run every 5 minutes (see above)
	 */
	public static function ilogistic_update_trigger() {
		if ( ! wp_next_scheduled( 'ilogistic_wc_update_order' ) ) {
			wp_schedule_event( time(), 'every_5_min', 'ilogistic_wc_update_order' );
		}
	}

	/**
	 * Register itself to the update event, get every order that changed in the last 5 minutes
	 */
	public static function update_ilog_order_event() {
		global $wpdb;
		$modified_orders = $wpdb->get_col( $wpdb->prepare(
			"SELECT posts.ID
                FROM {$wpdb->prefix}posts AS posts
                WHERE posts.post_type = 'shop_order'
                AND posts.post_modified >= '%s'
                AND posts.post_modified <= '%s'",
			date( 'Y/m/d H:i:s', absint( strtotime( '-5 MINUTES', current_time( 'timestamp' ) ) ) ),
			date( 'Y/m/d H:i:s', absint( current_time( 'timestamp' ) ) )
		) );
		foreach ( $modified_orders as $modified_order ) {
			$order                = new WC_Order( $modified_order );
			$is_product_empty_sku = false;
			foreach ( $order->get_items() as $item ) {
				$productData = new WC_Product( $item->get_data()['product_id'] );
				if ( $productData->get_sku() === "" ) {
					$is_product_empty_sku = true;
				}
				unset( $productData );
			}
			if ( $is_product_empty_sku ) {
				self::getLogger()->alert(
					"Failed to push order {$order->get_id()}, the order contains a product without SKU.",
					Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
				);
			} else {
				$ilog_order = Order::create_from_woocommerce_order( $order );
				$ilog_id    = $order->get_meta( 'ilogistic_id', true );
				Api_Service::getService()->update_order(
					$ilog_order,
					$ilog_id,
					esc_attr( get_option( 'ilogistic_token' ) )
				);
			}
		}
	}

	/**
	 * Deletes the order from the iLogistic side, when deleted from the shop.
	 * (It will do nothing when the order has been put into the bin.)
	 *
	 * @param int $order_id iLogistic order id
	 */
	public static function delete_order_event( int $order_id ) {
		global $post_type;

		if ( $post_type !== 'shop_order' ) {
			return;
		}

		$wc_order = new WC_Order( $order_id );
		$ilog_id  = $wc_order->get_meta( 'ilogistic_id', true );
		if ( $ilog_id !== "" ) {
			Api_Service::getService()->delete_order( $ilog_id, esc_attr( get_option( 'ilogistic_token' ) ) );
		}
	}

	/**
	 * Gets the orders that aren't completed yet and has ilogistic_id in the WooCommerce shop
	 * @return WC_Order[]
	 */
	public static function orders_needed_to_check(): array {
		$jovahagyva_state = get_option( 'ilogistic_status_jovahagyva' );
		$args             = array(
			'status' => [
				get_option( 'ilogistic_status_osszekeszites' ),
				get_option( 'ilogistic_status_csomagolva' ),
				// TODO: Add futarnak, when it's created on the ilog side!
			]
		);
		if ( is_array( $jovahagyva_state ) ) {
			$args = array_merge( $args['status'], $jovahagyva_state );
		} else {
			array_push( $args['status'], $jovahagyva_state );
		}
		$args['limit'] = -1;
		$orders = wc_get_orders( $args );
		foreach ( $orders as $key => $order ) {
			if ( $order->get_meta( 'ilogistic_id', true ) === "" ) {
				unset( $orders[ $key ] );
			}
		}

		return $orders;
	}

	/**
	 * Collects and push orders if:
	 * - `ilogistic_push_before_install` is enabled
	 * - order is in the iLogistic starting status
	 * - order has no `ilogistic_id` property
	 */
	public static function push_wc_order_created_before_install() {
		if ( get_option( 'ilogistic_push_before_install' ) === 'yes' ) {
			if ( is_array( get_option( 'ilogistic_status_jovahagyva' ) ) ) {
				$args = array(
					'status' => get_option( 'ilogistic_status_jovahagyva' )
				);
			} else {
				$args = array(
					'status' => [
						get_option( 'ilogistic_status_jovahagyva' )
					]
				);
			}
			$orders = wc_get_orders( $args );
			foreach ( $orders as $order ) {
				if ( $order->get_meta( 'ilogistic_id', true ) === "" ) {
					self::create_ilogistic_order( $order->get_id() );
				}
			}
		}
	}

	/**
	 * Updates the order statuses that are modified in the iLogistic side
	 */
	public static function update_wc_order_event() {
		$statuses    = array(
			//	'Jóváhagyva'          => get_option( 'ilogistic_status_jovahagyva' ),
			//	'Jóváhagyásra vár'    => get_option( 'ilogistic_status_jovahagyva' ),
			'Összekészítés alatt' => get_option( 'ilogistic_status_osszekeszites' ),
			'Csomagolva'          => get_option( 'ilogistic_status_csomagolva' ),
			'Futárnak átadva'     => get_option( 'ilogistic_status_futarnak' )
		);
		$wc_orders   = self::orders_needed_to_check();
		$api_service = Api_Service::getService();
		foreach ( $wc_orders as $order ) {
			$ilog_order = $api_service->get_order(
				$order->get_meta( 'ilogistic_id', true ),
				esc_attr( get_option( 'ilogistic_token' ) )
			);
			if ( $statuses[ $ilog_order['state'] ] !== $order->get_status() ) {
				if ( ! is_null( $statuses[ $ilog_order['state'] ] ) ) {
					$order->set_status( $statuses[ $ilog_order['state'] ] );
					$order->save();
				}
			}
		}
	}
}