<?php

namespace Ilogistic;

use WC_Log_Handler_File;

class Logfmt_Log_Handler_File extends WC_Log_Handler_File {

	/**
	 * Overrides the `WC_Log_Handler_File`'s default log formatter,
	 * to format the logs according to the logfmt 'specification'.
	 *
	 * @param int $timestamp Log timestamp.
	 * @param string $level emergency|alert|critical|error|warning|notice|info|debug.
	 * @param string $message Log message.
	 * @param array $context Additional information for log handlers.
	 *
	 * @return string Formatted log entry.
	 */
	protected static function format_entry( $timestamp, $level, $message, $context ): string {
		$final_message = array(
			'level'   => strtolower( $level ),
			'ts'      => date( 'Y-m-d\TH:i:s.u', $timestamp ),
			'message' => '"' . $message . '"',
		);
		$final_message = array_merge( $final_message, $context );

		return self::parseLogfmt( $final_message );
	}

	/**
	 * Marshall the given array to the logfmt format.
	 * @see https://www.brandur.org/logfmt
	 * @see https://pkg.go.dev/github.com/go-logfmt/logfmt
	 *
	 * @param array $logFields Potential key value pairs.
	 *
	 * @return string Marshalled logfmt string line.
	 */
	private static function parseLogfmt( array $logFields ): string {
		$parsedKeyValPairs = array();
		foreach ( $logFields as $key => $val ) {
			if ( is_int( $key ) ) {
				array_push( $parsedKeyValPairs, "{$val}=true" );
			}
			if ( is_array( $logFields[ $key ] ) || is_object( $logFields[ $key ] ) ) {
				foreach ( (array) $logFields[ $key ] as $subKey => $subValue ) {
					array_push( $parsedKeyValPairs, "{$key}.{$subKey}={$subValue}" );
				}
			} else {
				array_push( $parsedKeyValPairs, "{$key}={$logFields[$key]}" );
			}
		}

		return join( ' ', $parsedKeyValPairs );
	}

	/**
	 * Handle a log entry.
	 *
	 * @param int $timestamp Log timestamp.
	 * @param string $level emergency|alert|critical|error|warning|notice|info|debug.
	 * @param string $message Log message.
	 * @param array $context {
	 *      Additional information for log handlers.
	 *
	 * @type string $source Optional. Determines log file to write to. Default 'log'.
	 * @type bool $_legacy Optional. Default false. True to use outdated log format
	 *         originally used in deprecated WC_Logger::add calls.
	 * }
	 *
	 * @return bool False if value was not handled and true if value was handled.
	 */
	public function handle( $timestamp, $level, $message, $context ) {

		if ( isset( $context['source'] ) && $context['source'] ) {
			$handle = $context['source'];
		} else {
			$handle = 'log';
		}

		$entry = self::format_entry( $timestamp, $level, $message, $context );

		return $this->add( $entry, $handle );
	}
}