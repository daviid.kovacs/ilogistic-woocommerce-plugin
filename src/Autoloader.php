<?php

namespace Ilogistic;

if ( ! defined( 'ABSPATH' ) ) {
	die;
}


class Autoloader {

	private $plugin_namespace = 'Ilogistic';
	private $plugin_path;

	/**
	 * Autoloader constructor.
	 *
	 * @param $plugin_path string Absolute path to the plugin
	 */
	public function __construct( string $plugin_path ) {
		$this->plugin_path = $plugin_path;

		$this->register();
	}

	/**
	 * Unregister the autoloader
	 */
	public function unregister() {
		spl_autoload_unregister( array( $this, 'autoloader' ) );
	}

	/**
	 * Registers a new autoloader
	 *
	 * @param bool $prepend Push to the start of the autoloader stack instead of the end
	 */
	public function register( $prepend = true ) {
		spl_autoload_register( array( $this, 'autoloader' ), true, $prepend );
	}

	/**
	 * Autoloader function, will turn the fully qualified class name into a file name, and require it,
	 * according to the PSR-4 specification.
	 * For example: class 'Ilogistic\Model\Product' to '/folder/Model/Product.php'.
	 *
	 * @param $class string Fully qualified class name with namespace and everything.
	 */
	public function autoloader( string $class ) {
		$path_parts = explode( '\\', $class );

		if ( count( $path_parts ) <= 1 ) {
			return;
		}

		$start = array_values( $path_parts )[0];

		if ( $start !== $this->plugin_namespace ) {
			return;
		}

		$file = $this->plugin_path;

		$end = array_pop( $path_parts );

		foreach ( $path_parts as $part ) {
			if ( $part !== $this->plugin_namespace ) {
				preg_match_all( '!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $part, $matches );

				$parts = $matches[0];

				$part = implode( '_', $parts );

				$file .= $part . '/';
			}
		}
		preg_match_all( '!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $end, $matches );

		$end = $matches[0];

		$end = implode( '_', $end );

		$file_ending = $end . '.php';

		if ( is_file( $file . $file_ending ) ) {
			require_once( $file . $file_ending );

			return;
		}

	}
}
