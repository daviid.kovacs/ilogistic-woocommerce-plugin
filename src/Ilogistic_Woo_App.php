<?php

namespace Ilogistic;

use Ilogistic\Hooks\Admin_Menu_Hooks;
use Ilogistic\Hooks\Order_Hooks;
use Ilogistic\Hooks\Product_Hooks;
use Ilogistic\Hooks\Authentication_Hooks;

class Ilogistic_Woo_App {

	public const ILOGISTIC_PLUGIN_VERSION = '1.6.7';

	public const DEFAULT_LOGGER_CONTEXT = [
		'source'  => 'iLogistic',
		'version' => self::ILOGISTIC_PLUGIN_VERSION
	];

	private static $instance = null;

	/**
	 * Creates a new iLogistic Application
	 * @return Ilogistic_Woo_App
	 */
	public static function get_instance(): Ilogistic_Woo_App {
		if ( is_null( self::$instance ) ) {
			self::$instance = new Ilogistic_Woo_App();
		}

		return self::$instance;
	}

	/**
	 * Checks if the woocommerce plugin is activated
	 * @return bool Returns true when the WC plugin is active, else false
	 */
	private static function check_wc_plugin(): bool {
		return (
			class_exists( 'WooCommerce' )
			|| in_array(
				'woocommerce/woocommerce.php',
				apply_filters( 'active_plugins', get_option( 'active_plugins' ) )
			)
			|| is_plugin_active( 'woocommerce/woocommerce.php' )
			|| is_plugin_active_for_network( 'woocommerce/woocommerce.php' )
			|| is_plugin_active( '__woocommerce/woocommerce.php' )
			|| is_plugin_active_for_network( '__woocommerce/woocommerce.php' )
		);
	}

	/**
	 * Emits a WP error for the user when the plugin is activated without the Woocommerce plugin.
	 */
	public static function activation_handler(): void {
		if ( ! self::check_wc_plugin() ) {
			wp_die( 'Sajnáljuk, de a bővítmény(plugin) használatához előbb telepítened kell ' .
			        'és be kell kapcsolnod a Woocommerce bővítményt ' .
			        '<br><a href="' . admin_url( 'plugins.php' ) . '">&laquo; Vissza a Bővítményekhez</a>'
			);
		}
		if (
			get_option( 'ilogistic_token', null ) !== null &&
			get_option( 'ilogistic_token', null ) !== "" &&
			Api_Service::getService()->validate_token( get_option( 'ilogistic_token', null ) )
		) {
			Product_Hooks::register_all_products();
		}
	}

	/**
	 * Unregister the created hooks/crons, when disabled.
	 */
	public static function deactivation_handler() {
		Hook_Registrator::unregister_hooks();
	}

	/**
	 * Registers the plugin hooks.
	 */
	public function run() {
		Hook_Registrator::register_hooks(
			Order_Hooks::class,
			Admin_Menu_Hooks::class,
			Product_Hooks::class,
			Authentication_Hooks::class
		);
	}

}