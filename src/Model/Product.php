<?php


namespace Ilogistic\Model;


use WC_Product;

class Product {
	public $itemNumber;
	public $barCode;
	public $name;
	public $type;
	public $category;
	public $weight;
	public $width;
	public $height;
	public $depth;
	public $fragile;
	public $price;
	public $tax;
	public $critical;
	public $status;

	/**
	 * @param $wc_product WC_Product Product from the WooCommerce database.
	 */
	private function __construct( WC_Product $wc_product ) {
		$this->itemNumber = (string) $wc_product->get_sku();
		$this->barCode    = "";
		$this->name       = $wc_product->get_name();
		$this->type       = rtrim(
			explode( '>', wc_get_product_category_list( $wc_product->get_id() ) )[1],
			'</a'
		);
		$this->category   = $wc_product->is_virtual() ? 'virtual' : 'termek';
		$this->status     = "active";
		$this->weight     = $wc_product->get_weight() === "" ? 0 : $wc_product->get_weight();
		$this->width      = $wc_product->get_width() === "" ? 0 : $wc_product->get_width();
		$this->height     = $wc_product->get_height() === "" ? 0 : $wc_product->get_height();
		$this->depth      = $wc_product->get_length() === "" ? 0 : $wc_product->get_length();
		$this->fragile    = 1;
		$this->price      = $wc_product->get_price() === "" ? 0 : $wc_product->get_price();
		$this->tax        = 27;
		$this->critical   = 0;
	}

	/**
	 * Creates a new Product from based on the Woocommerce data.
	 *
	 * @param $wc_product WC_Product product from the WC database
	 *
	 * @return Product new Product based on the input data
	 */
	public static function create_from_woocommerce_product( WC_Product $wc_product ): Product {
		return new Product( $wc_product );
	}

	/**
	 * Returns the Product as json string, that compatible with the iLogistic API.
	 * @return string Json String
	 */
	public function get_as_json(): string {
		return json_encode( $this, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT );
	}

}