<?php


namespace Ilogistic\Model;


use Ilogistic\Ilogistic_Woo_App;
use Ilogistic\Logfmt_Log_Handler_File;
use WC_Logger;
use WC_Order;
use WC_Product;

/**
 * Class Order
 * @package Cyclick\Ilogistic\Model
 */
class Order {
	/** @var null|string $foreignId WC_Order ID, marked as foreignId in the iLogistic system */
	public $foreignId = null;
	public $delivery = array();
	public $billing = array();
	public $payment = array();
	public $content = array();

	/** @var null|WC_Logger $logger */
	private static $logger = null;

	/**
	 * Order constructor.
	 *
	 * @param $input_data WC_Order|array Can container a WC_Order object or and Assoc array with the API data
	 * @param $is_woocommerce_data bool Indicates if the $inputData is from the WC or not
	 */
	private function __construct( $input_data, bool $is_woocommerce_data ) {
		if ( $is_woocommerce_data ) {
			$this->create_from_woocommerce_data( $input_data );
		} else {
			$this->create_from_api_data( $input_data );
		}
	}

	/**
	 * Parses the API data to self's public fields
	 *
	 * @param $input_data WC_Order Woocommerce Order object that contains the order's data
	 */
	private function create_from_woocommerce_data( WC_Order $input_data ) {
		if ( $input_data->meta_exists( 'foxpost_woo_parcel_apt_id' ) ) {
			$parcel_number = $input_data->get_meta( 'foxpost_woo_parcel_apt_id', true );
		} else {
			$parcel_number = "";
		}
		$this->foreignId = (string) $input_data->get_id();
		$this->delivery  = [
			"name"        => $input_data->get_shipping_first_name() . " " . $input_data->get_shipping_last_name(),
			"email"       => $input_data->get_billing_email(),
			"phoneNumber" => $input_data->get_billing_phone(),
			"country"     => $input_data->get_shipping_country(),
			"postCode"    => $input_data->get_shipping_postcode(),
			"city"        => $input_data->get_shipping_city(),
			"address"     => $input_data->get_shipping_address_1() . " " . $input_data->get_shipping_address_2(),
			"company"     => $this->prepare_shipping_method( $input_data->get_shipping_method() ),
			"aptNumber"   => $parcel_number
		];
		$this->billing   = [
			"name"        => $input_data->get_billing_first_name() . " " . $input_data->get_billing_last_name(),
			"email"       => $input_data->get_billing_email(),
			"phoneNumber" => $input_data->get_billing_phone(),
			"country"     => $input_data->get_billing_country(),
			"postCode"    => $input_data->get_billing_postcode(),
			"city"        => $input_data->get_billing_city(),
			"address"     => $input_data->get_billing_address_1() . " " . $input_data->get_billing_address_2()
		];
		$this->payment   = [
			"type" => $this->prepare_payment_method( $input_data->get_payment_method_title() ),
			"cost" => $input_data->calculate_totals()
		];
		foreach ( $input_data->get_items() as $item ) {
			$productData = new WC_Product( $item->get_data()['product_id'] );
			array_push( $this->content, [
				"itemNumber" => $productData->get_sku(),
				"quantity"   => $item->get_quantity()
			] );
		}
	}

	/**
	 * Parses the API data to self's public fields
	 *
	 * @param $input_data array Assoc array contains the API data
	 */
	private function create_from_api_data( array $input_data ) {
		$this->delivery = [
			"name"        => $input_data["delivery"]["name"],
			"email"       => $input_data["delivery"]["email"],
			"phoneNumber" => $input_data["delivery"]["phoneNumber"],
			"country"     => $input_data["delivery"]["country"],
			"postCode"    => $input_data["delivery"]["postCode"],
			"city"        => $input_data["delivery"]["city"],
			"address"     => $input_data["delivery"]["address"],
			"company"     => $input_data["delivery"]["company"],
			"aptNumber"   => $input_data["delivery"]["aptNumber"]
		];
		$this->billing  = [
			"name"        => $input_data["billing"]["name"],
			"email"       => $input_data["billing"]["email"],
			"phoneNumber" => $input_data["billing"]["phoneNumber"],
			"country"     => $input_data["billing"]["country"],
			"postCode"    => $input_data["billing"]["postCode"],
			"city"        => $input_data["billing"]["city"],
			"address"     => $input_data["billing"]["address"]
		];
		$this->payment  = [
			"type" => $input_data["payment"]["type"],
			"cost" => $input_data["payment"]["cost"]
		];
		foreach ( $input_data["content"] as $item ) {
			array_push( $this->content, [ "itemNumber" => (string) $item["sku"], "quantity" => $item["quantity"] ] );
		}
	}

	/**
	 * Creates a new Order from based on the Woocommerce data.
	 *
	 * @param $db_data WC_Order order from the WC database
	 *
	 * @return Order new Order based on the input data
	 */
	public static function create_from_woocommerce_order( WC_Order $db_data ): Order {
		return new Order( $db_data, true );
	}

	/**
	 * @param $db_data array Order that comes from the iLogistic API
	 *
	 * @return Order new Order based on the input data
	 */
	public static function create_from_api_request( array $db_data ): Order {
		return new Order( $db_data, false );
	}

	/**
	 * Returns the Order as json string, that compatible with the iLogistic API.
	 * @return string Json String
	 */
	public function get_as_json(): string {
		return json_encode( $this, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT );
	}

	/**
	 * Converts the WooCommerce shipping method to an iLogistic one, mapping by the settings fields.
	 *
	 * @param string $wc_shipping_method WooCommerce shipping method's title.
	 *
	 * @return string iLogistic compatible shipping method or the original title for backwards compatibility.
	 */
	private function prepare_shipping_method( string $wc_shipping_method ): string {
		$gls_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_gls' ) ) );

		$foxpost_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_foxpost' ) ) );

		if ( $wc_shipping_method === '' ) {
			return 'Nem szükséges';
		}
		if ( in_array( $wc_shipping_method, $gls_shippings, true ) ) {
			return 'GLS';
		}
		if ( in_array( $wc_shipping_method, $foxpost_shippings, true ) ) {
			return 'Foxpost';
		}

		self::getLogger()->warning(
			"Order {$this->foreignId} has possible bad shipping method, please check your settings.",
			Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
		);

		return $wc_shipping_method;
	}

	/**
	 * Converts the WooCommerce payment method to an iLogistic one, mapping by the settings fields.
	 *
	 * @param string $wc_payment_method WooCommerce payment status.
	 *
	 * @return string iLogistic compatible payment method or the original title for backwards compatibility.
	 */
	private function prepare_payment_method( string $wc_payment_method ): string {
		$card_payment = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_payment_card' ) ) );

		$cod = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_payment_cod' ) ) );

		$bank_transfer = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_payment_transfer' ) ) );

		if ( in_array( $wc_payment_method, $card_payment, true ) ) {
			return 'Bankkártyás fizetés';
		}
		if ( in_array( $wc_payment_method, $cod, true ) ) {
			return 'Utánvétes fizetés';
		}
		if ( in_array( $wc_payment_method, $bank_transfer, true ) ) {
			return 'Átutalásos fizetés';
		}

		self::getLogger()->warning(
			"Order {$this->foreignId} has possible bad payment method, please check your settings.",
			Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
		);

		return $wc_payment_method;
	}

	/**
	 * Creates a new WC_Logger or returns and existing one.
	 * @return WC_Logger
	 */
	private static function getLogger(): WC_Logger {
		if ( is_null( self::$logger ) ) {
			$log_handler  = new Logfmt_Log_Handler_File();
			self::$logger = new WC_Logger( [ $log_handler ] );
		}

		return self::$logger;
	}

}