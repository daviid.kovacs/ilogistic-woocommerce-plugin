<?php

namespace Ilogistic;

use Ilogistic\Model\Order;
use Ilogistic\Model\Product;
use WC_Logger;
use WP_Error;

class Api_Service {

	const MULTIPLE_PRODUCTS_API_URL = 'https://api.ilogistic.eu/products/products';
	const PUSH_MULTIPLE_PRODUCTS_API_URL = 'https://api.ilogistic.eu/products/products-multiple';
	const MULTIPLE_ORDERS_API_URL = 'https://api.ilogistic.eu/orders/orders';
	const SINGLE_ORDER_API_URL = 'https://api.ilogistic.eu/orders/order/';
	const AUTHENTICATION_API_URL = 'https://api.ilogistic.eu/authentication/auth';
	const TOKEN_VALIDATION_API_URL = 'https://api.ilogistic.eu/authentication/validate';
	const STOCK_PRODUCTS_API_URL = 'https://api.ilogistic.eu/products/stock';

	private static $instance = null;
	private $logger;

	private function __construct() {
		$log_handler  = new Logfmt_Log_Handler_File();
		$this->logger = new WC_Logger( [ $log_handler ] );
	}

	/**
	 * @return Api_Service
	 */
	public static function getService(): Api_Service {
		if ( self::$instance === null ) {
			self::$instance = new Api_Service();
		}

		return self::$instance;
	}

	/**
	 * @param $order Order
	 * @param int $order_id
	 * @param $token string JWT token
	 *
	 * @return int
	 */
	public function push_order( Order $order, int $order_id, string $token ): int {
		$body     = $order->get_as_json();
		$response = wp_remote_post(
			self::MULTIPLE_ORDERS_API_URL,
			array(
				'method'      => 'POST',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'blocking'    => true,
				'body'        => $body
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}

			return 0;
		}
		$unmarshalledResponse = json_decode( $response['body'], true );
		if ( is_null( $unmarshalledResponse ) ) {
			$this->logger->alert(
				"Failed to push order {$order_id}, error: {$response['body']}",
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);
		} else {
			$this->logger->info(
				"Pushed the order {$order_id}.",
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);
		}

		return ! is_null( $unmarshalledResponse['id'] ) ? $unmarshalledResponse['id'] : 0;
	}

	public function get_order( string $ilog_id, string $token ) {
		$response = wp_remote_get(
			self::SINGLE_ORDER_API_URL . $ilog_id,
			array(
				'method'      => 'GET',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'blocking'    => true
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}

			return null;
		} else {
			$this->logger->info(
				"Pulled an order {$ilog_id}.",
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);

			return json_decode( $response['body'], true );
		}
	}

	/**
	 * @param $product Product
	 * @param $token string JWT token
	 */
	public function push_product( Product $product, string $token ) {
		$body     = $product->get_as_json();
		$response = wp_remote_post(
			self::MULTIPLE_PRODUCTS_API_URL,
			array(
				'method'      => 'POST',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'blocking'    => true,
				'body'        => $body
			) );
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}
		} else {
			$ilog_product_id = json_decode( $response["body"], true )["id"];
			$this->logger->info(
				"Pushed a product {$ilog_product_id}.",
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);
		}
	}

	/**
	 * @param $products Product[]
	 * @param $token string
	 */
	public function push_products( array $products, string $token ) {
		$productIds = array_reduce( $products, function ( string $accu, Product $currentProduct ) {
			return $accu . $currentProduct->itemNumber . ", ";
		}, '' );
		$body       = json_encode( $products, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT );
		$response   = wp_remote_post(
			self::PUSH_MULTIPLE_PRODUCTS_API_URL,
			array(
				'method'      => 'POST',
				'timeout'     => 20,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'blocking'    => true,
				'body'        => $body
			)
		);
		$this->logger->info(
			"Pushed the following products to the API: {$productIds}",
			Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}
		}
	}

	/**
	 * @param Order $order
	 * @param string $ilog_id
	 * @param string $token
	 */
	public function update_order( Order $order, string $ilog_id, string $token ) {
		$response = wp_remote_post(
			self::SINGLE_ORDER_API_URL . $ilog_id,
			array(
				'method'      => 'PATCH',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'blocking'    => true,
				'body'        => $order->get_as_json()
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}
		} else {
			$this->logger->info(
				"Updated an order {$ilog_id}.",
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);
		}
	}

	public function get_new_token( string $token ): string {
		$response = wp_remote_get(
			self::AUTHENTICATION_API_URL,
			array(
				'method'      => 'GET',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => [ 'Authorization' => 'Bearer ' . $token, 'Accept' => 'application/json' ],
				'blocking'    => true,
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}
			$this->logger->critical(
				'Failed to pull a new token form iLogistic, yours will expire soon. ' .
				'Please contact the developers or generate a new token manually!',
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);

			return $token;
		}
		$unmarshalledResponse = json_decode( $response['body'], true );
		$this->logger->info(
			'Your Authentication token renewed automatically.',
			Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
		);
		if ( isset( $unmarshalledResponse['error'] ) ) {
			$this->logger->critical(
				'Failed to pull a new token form iLogistic and yours will expire soon. ' .
				'Please contact the developers or generate a new token manually! Error: ' .
				$unmarshalledResponse['error']
			);
		}

		if ( isset( $unmarshalledResponse['token'] ) ) {
			return $unmarshalledResponse['token'];
		} else {
			return '';
		}
	}

	public function delete_order( string $ilog_id, string $token ) {
		$response = wp_remote_post(
			self::SINGLE_ORDER_API_URL . $ilog_id,
			array(
				'method'      => 'DELETE',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => [ 'Authorization' => 'Bearer ' . $token ],
				'blocking'    => true,
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}
		}
		$this->logger->info(
			"Order deleted {$ilog_id}",
			Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
		);
	}

	public function validate_token( string $token ): bool {
		$response = wp_remote_get(
			self::TOKEN_VALIDATION_API_URL,
			array(
				'method'      => 'GET',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => [ 'Authorization' => 'Bearer ' . $token ],
				'blocking'    => true
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}
		}
		$message = json_decode( $response['body'], true );
		if ( isset( $message['error'] ) ) {
			$this->logger->warning(
				"Failed to validate token, {$message['error']}",
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);

			return false;
		} else {
			$this->logger->info(
				'Authentication token validated.',
				Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
			);

			return true;
		}
	}

	/**
	 * Get the current stock of the given skus.
	 *
	 * @param array $updatable_products Numeric array contains updatable products
	 * @param string $token iLogistic API token
	 *
	 * @return array Unmarshalled API response ['sku' => stock, 'sku2' => stock ...]
	 */
	public function get_stock( array $updatable_products, string $token ): array {
		$response = wp_remote_post(
			self::STOCK_PRODUCTS_API_URL,
			array(
				'method'      => 'POST',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'body'        => json_encode( $updatable_products, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE ),
				'blocking'    => true
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}

			return [];
		}
		$stocks = json_decode( $response['body'], true );
		if ( is_null( $stocks ) ) {
			return [];
		}

		return $stocks;
	}

	/**
	 * Emits a Woocommerce error in the log with the HTTP request failure.
	 *
	 * @param string $error_message HTTP error message
	 */
	private function log_error( string $error_message ) {
		$this->logger->error(
			"Failed to process HTTP request! {$error_message}",
			Ilogistic_Woo_App::DEFAULT_LOGGER_CONTEXT
		);
	}

	/**
	 * Parses the headers as an associative array.
	 *
	 * @param string $token Authorization token
	 *
	 * @return string[] Array of HTTP request headers
	 */
	private function prepare_headers( string $token ): array {
		return array(
			'Content-Type'  => 'application/json',
			'Accept'        => 'application/json',
			'Authorization' => 'Bearer ' . $token
		);
	}
}